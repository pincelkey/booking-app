<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cita extends Model {
    protected $table    = 'wp_citas';
    protected $fillable = [
        'title',
        'phone',
        'email',
    ];
}
