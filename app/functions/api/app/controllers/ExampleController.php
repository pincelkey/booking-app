<?php

namespace App\Controllers;

use App\Models\Example;
use App\Models\Cita;
use Exception;

class ExampleController {
    public function __construct() { }

    public function index($request) {
        $examples = Example::all();

        if (count($examples)) {
            return $examples;
        } else {
            throw new Exception('No examples found');
        }
    }

    public function storeCitas($request) {
        $cita = new Cita();

        $cita->title = $request['title'];
        $cita->phone = $request['phone'];
        $cita->email = $request['email'];

        $cita->save();

        return $cita;
    }

    public function showHoursByDay($request) {
        $services = get_posts([
            'name'      => $request['service'],
            'post_type' => 'service'
        ]);

        if (count($services) > 0) {
            $service    = $services[0];
            $day        = $request['day'];

            return get_field($day, $service->ID);
        } else {
            return false;
        }
    }
}
